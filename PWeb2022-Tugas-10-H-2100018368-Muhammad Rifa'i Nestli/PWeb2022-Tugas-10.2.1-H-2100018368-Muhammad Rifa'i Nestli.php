<html>
    <head>
        <title>Pweb-Tugas 10-2100018368</title>
    </head>
    <body>
        <form method="post" action="">
            <b>Program Pengkonversian Nilai</b><br><br>
            Masukkan nilai :<br>
            <input type="text" name="nilai" placeholder="Angka 0-100"><br><br>
            &emsp;<input type="submit" name="kirim" value="Submit">
        </form>
    <?php
        echo "<br><b>Hasil Konversi Nilai : </b><br><br>";
        $huruf=" ";
        $numerik=" ";
        if (isset($_POST['kirim'])) {
            $nilai = $_POST['nilai'];
            if ($nilai >= 80 && $nilai <= 100) {
                $huruf="A";
                $numerik="4.00";
            } 
            else if ($nilai >= 76.25 && $nilai <= 79.99) {
                $huruf="A-";
                $numerik="3.67";
            } 
            else if ($nilai >= 68.75 && $nilai <= 76.24) {
                $huruf="B+";
                $numerik="3.33";
            } 
            else if ($nilai >= 65 && $nilai <= 68.74) {
                $huruf="B";
                $numerik="3.00";
            } 
            else if ($nilai >= 62.50 && $nilai <= 64.99) {
                $huruf="B-";
                $numerik="2.67";
            } 
            else if ($nilai >= 57.50 && $nilai <= 62.49) {
                $huruf="C+";
                $numerik="2.33";
            }
            else if ($nilai >= 55.00 && $nilai <= 57.49) {
                $huruf="C";
                $numerik="2.00";
            } 
            else if ($nilai >= 51.25 && $nilai <= 54.99) {
                $huruf="C-";
                $numerik="1.67";
            } 
            else if ($nilai >= 43.75 && $nilai <= 51.24) {
                $huruf="D+";
                $numerik="1.33";
            } 
            else if ($nilai >= 40 && $nilai <= 43.74) {
                $huruf="D";
                $numerik="1.00";
            } 
            else if ($nilai >= 0 && $nilai <= 39.99) {
                $huruf="E";
                $numerik="0";
            }  
            else {
                echo "Nilai yang dimasukkan tidak valid !<br><br>";
            }
            echo "Nilai yang dimasukkan : $nilai<br>";
            echo "Nilai Huruf   : $huruf <br>";
            echo "Nilai Numerik : $numerik <br>";
        }
    ?>
    </body>
</html>
