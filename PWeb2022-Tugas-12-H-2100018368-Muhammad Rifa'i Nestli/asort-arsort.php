<?php
    $arrUmur=array("Jayadi"=>40, "pai"=>28, "dena"=>25, "jalul"=>30);
    echo "<b>Array sebelum diurutkan</b>";
    echo "<pre>";
    print_r($arrUmur);
    echo "</pre>";

    asort($arrUmur);
    reset($arrUmur);
    echo "<b>Array setelah diurutkan dengan asort()</b>";
    echo "<pre>";
    print_r($arrUmur);
    echo "</pre>";

    arsort($arrUmur);
    reset($arrUmur);
    echo "<b>Array setelah diurutkan dengan arsort()</b>";
    echo "<pre>";
    print_r($arrUmur);
    echo "</pre>";
?>