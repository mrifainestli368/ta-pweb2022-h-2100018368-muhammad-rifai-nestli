<?php 
    $arrumur=array("Jayadi"=>15, "pai"=>19, "dena"=>25, "jalul"=>30); 
    echo "Menampilkan isi array asosiatif dengan foreach: <br>"; 
    foreach ($arrumur as $nama=> $umur){ 
        echo "Umur $nama=$umur<br>"; 
    }
    reset($arrumur);
    echo "<br>Menampilkan isi array asosiatif dengan WHILE dan LIST: <br>"; 
    while(list($nama, $umur) = each($arrumur)){ 
        echo "Umur $nama=$umur<br>";
    }
?>