<?php
    $arrUmur=array("Jayadi"=>40, "pai"=>28, "dena"=>25, "jalul"=>30);
    echo "<b>Array sebelum diurutkan</b>";
    echo "<pre>";
    print_r($arrUmur);
    echo "</pre>";

    ksort($arrUmur);
    reset($arrUmur);
    echo "<b>Array setelah diurutkan dengan ksort()</b>";
    echo "<pre>";
    print_r($arrUmur);
    echo "</pre>";

    krsort($arrUmur);
    reset($arrUmur);
    echo "<b>Array setelah diurutkan dengan krsort()</b>";
    echo "<pre>";
    print_r($arrUmur);
    echo "</pre>";
?>