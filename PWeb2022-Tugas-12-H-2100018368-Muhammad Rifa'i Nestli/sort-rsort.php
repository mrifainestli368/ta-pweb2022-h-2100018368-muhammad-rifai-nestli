<?php
$arrUmur=array("Jayadi"=>40, "pai"=>28, "dena"=>25, "jalul"=>30);
echo "<b>Array sebelum diurutkan</b>";
echo "<pre>";
print_r($arrUmur);
echo "</pre>";

sort($arrUmur);
reset($arrUmur);
echo "<b>Array setelah diurutkan dengan sort()</b>";
echo "<pre>";
print_r($arrUmur);
echo "</pre>";

rsort($arrUmur);
reset($arrUmur);
echo "<b>Array setelah diurutkan dengan rsort()</b>";
echo "<pre>";
print_r($arrUmur);
echo "</pre>";
?>