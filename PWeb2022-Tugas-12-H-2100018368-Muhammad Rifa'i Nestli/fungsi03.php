<?php
//Fungsi dengan return value dan parameter
function volume_lingkaran ($radius){ 
    return 4/3*3.14*$radius*$radius*$radius;
}
//Pemanggilan fungsi
    $r=15;
    echo "Volume bola dengan radius $r = "; 
    echo volume_lingkaran($r);
?>